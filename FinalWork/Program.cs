﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Microsoft.Win32;
using KeyHandler;

/*! \mainpage
\author Juha Kallioniemi - 1501818
\author Ilari Kahila - 1501834
\date 10.12.2017
*/


/// <summary>
/// Program Handles the keys through KeyHandler.dll
/// </summary>
namespace FinalWork
{
    /// <summary>
    ///  Handles arguments and informs if program is in debug or release 
    /// </summary>
    class Program
    { 
        static void Main(string[] args)
        {
#if DEBUG
            Console.WriteLine("-------------------------------------------------------------------");
            Console.WriteLine("---------------------------Debug version---------------------------");
            Console.WriteLine("-------------------------------------------------------------------");
#else
            Console.WriteLine("--------------------------------------------------------------------");
            Console.WriteLine("--------------------------Release version---------------------------");
            Console.WriteLine("--------------------------------------------------------------------");
#endif


            if (args.Length > 0)
            {

                string[] arg = Environment.GetCommandLineArgs();
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    { 
                        case "-a":
                            HandleKeys.SetKeys(Constants.keyName);
                            Console.WriteLine("Keys set!");
                            break;
                        case "-d":
                            Registry.LocalMachine.DeleteSubKey(Constants.subkey);
                            Console.WriteLine(Constants.subkey + " - subkey deleted!");
                            break;
                        case "-r":
                            HandleKeys.GetKeys(Constants.keyName);
                            break;
                        case "-h":
                            PrintArguments();
                            break;
                        default:
                            Console.WriteLine("No such arguments. all available arguments listed below:");
                            PrintArguments();
                            break;
                    }
                }
            } else
            {
                Console.WriteLine("No arguments given. all available arguments listed below:");
                PrintArguments();
            }
        }

        static void PrintArguments()
        {
            Console.WriteLine("-a sets the keys.");
            Console.WriteLine("-r reads the keys.");
            Console.WriteLine("-d deletes the keys.");
            Console.WriteLine("-h lists all commands.");
            Console.WriteLine();
        }
    }

    /// <summary>
    /// Constants are presented here.
    /// </summary>
    public static class Constants {

        public const string userRoot = "HKEY_LOCAL_MACHINE"; 
        public const string subkey = "SOFTWARE\\Ilari_Kahila_Juha_Kallioniemi"; 
        public const string keyName = userRoot + "\\" + subkey; 

    }

}
