Authors:
	Ilari Kahila - 1501834
	Juha Kallioniemi - 1501818

Program Description:
	When starting the program it gives you command line arguments that you can use when calling the program.
	
	Here they are listed anyway:
		-a sets the keys.
		-r reads the keys.
		-d deletes the keys.
		-h lists all commands.

"BaseForKeyHandler" folder includes a cs file from which the keyHandler.dll file was made of.