﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace KeyHandler
{
    public class HandleKeys
    {
        public static void SetKeys(string keyName)
        {
            ///
            /// An int value can be stored without specifying the registry data type, but long values will be 
            /// 
            
            Registry.SetValue(keyName, "", 1234);
            Registry.SetValue(keyName, "TestLong", 12345678901234,
                RegistryValueKind.QWord);

            /// Test String with ecpandable enviroment variable
            Registry.SetValue(keyName, "TestExpand", "My path: %path%");
            Registry.SetValue(keyName, "TestExpand2", "My path: %path%",
                RegistryValueKind.ExpandString);
        }

        public static void GetKeys(string keyName)
        {
            /// Returns this value if name/value pair not found
            string noSuch = (string)Registry.GetValue(keyName, "NoSuchName", "Return this default if NoSuchName does not exist.");
            Console.WriteLine("\r\nNoSuchName: {0}", noSuch);

            /// Getting the integer and long values
            int tInteger = (int)Registry.GetValue(keyName, "", -1);
            Console.WriteLine("(Default): {0}", tInteger);
            long tLong = (long)Registry.GetValue(keyName, "TestLong", long.MinValue);
            Console.WriteLine("TestLong: {0}", tLong);


            ///A string with embedded environment variables is not
            ///expanded if it was stored as an ordinary string.
            string tExpand = (string)Registry.GetValue(keyName,
                "TestExpand",
                "Default if TestExpand does not exist.");
            Console.WriteLine("TestExpand: {0}", tExpand);

            /// A string stored as ExpandString is expanded.
            string tExpand2 = (string)Registry.GetValue(keyName,
                "TestExpand2",
                "Default if TestExpand2 does not exist.");
            Console.WriteLine("TestExpand2: {0}...",
                    tExpand2.Substring(0, 40));

        }
    }
}
